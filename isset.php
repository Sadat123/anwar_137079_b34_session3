<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 9/21/2016
 * Time: 2:56 PM
 */
$var = '0';

// This will evaluate to TRUE so the text will be printed.
if (isset($var)) {
    echo "This var is set so I will print.";
}
echo "</br>";
$a = "test";
$b = "anothertest";

var_dump(isset($a));      // TRUE
var_dump(isset($a, $b)); // TRUE
echo "</br>";
unset ($a);

var_dump(isset($a));     // FALSE
var_dump(isset($a, $b)); // FALSE
echo "</br>";
$foo = NULL;
var_dump(isset($foo));   // FALSE
